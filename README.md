# Collecting domain specific data given a seed set of structured and unstructured sources (TEAM 6)

* Domain: Computer Scientists

## NOTE: Please refer to REPORT for all the details.
The README only contains a brief explanation of directory structure. Refer to REPORT for other details.

### Directory Structure (highlights only the main files ONLY even though other files might be present as well)

```
essential_files/
├── alias_mapping.json
├── FINAL.json
└── INITIAL_WIKI_SEED_NAMES_LIST.json

scrapers_and_crawlers/ # contains all the code written to scrape an crawl


analysis_merging_cleaning/
├── 01 create_mappings_to_other_databases
│   ├── create_EXT_databases_mapping.ipynb   # extracts mapping from wikipedia to different databases
│   ├── dblp_mapping.json
│   ├── google_scholar_mapping.json
│   ├── math_genea_mapping.json
│   ├── VIAF_mapping.json
│   └── world_cat.json


├── 02 explore_wiki_data
│   ├── all_funcs.py   #  [IMP] contains modules to clean the different values
│   ├── all_wiki_attributes_freq_sorted.json  # contains relevant PRELIM attributes from only one DATASET ie wikipedia
│   ├── nice_birth_date_mapping.json # a file showing the decent mapping of DOB using the heuristic
│   ├── nov_explore_wikidata.ipynb  # judging usefulness of attributes in WIKIDATA
│   ├── nov_fetch_aliases.ipynb  #  [IMP] TO CREATE AN ALIAS MAPPING
│   ├── oct_explore_attributes.ipynb
│   ├── purify_functions.py  # [IMP] contains mapping for which attribute needs to be purified how and to which parent
│   ├── test_purify_functions.ipynb  # [IMP] purifies the wikipedia data using purify_functions.py
│   └── useful_attributes.json    # the attributes distribution at the earliest stage


├── 03 reexplore_attributes
│   ├── intro_counter.ipynb  # introducing "data","num_sources" AND "is_reliable"
│   ├── night_attr_analyze.json
│   └── nov_night_analyze_attrs.ipynb


├── 04 purify_all_stuff
│   ├── all_funcs.py #  [IMP] contains modules to clean the different values
│   ├── del_attributes_list.json  # attributes to be deleted due to sparsity or irrelevance
│   ├── oct_create_prelim_wiki_data.ipynb
│   ├── prune_dataframe.ipynb  # [IMP] main code to PRUNE ALL DATA, mapping, merging etc
│   ├── prune_helper.py  # [IMP] helper functions for mapping and merging
│   ├── sample_and_freqs_for_attributes.json # [IMP] final description for all attributes 
│   ├── store_sorted_freqs.ipynb
│   ├── test_names.py       # to find common tokens between two tokens
│   └── update_sorted_stuff.py  


└── 05 plots
    └── plot.ipynb



```